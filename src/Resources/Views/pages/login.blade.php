@extends('crm-guard::layouts.master')

@section('main')
    <div id="app_login" logged_in="{!! $logged_in !!}" settings='{!! $settings !!}'>
        <LoginPage />
    </div>
@stop