<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

    <meta name="csrf_token" content="{!! csrf_token() !!}"/>
    <meta name="csrf-token" content="{!! csrf_token() !!}"/>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">

    @stack('headcss')

    @yield('meta')

</head>
<body>

<div class="tint">
    @yield('main')
</div>

<script src="/crm-guard/js/vue-login.js"></script>

@stack('scripts')

</body>
</html>
