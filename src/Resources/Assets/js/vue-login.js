import LoginPage from "../../Vue/LoginPage.vue";
import "vuetify/dist/vuetify.min.css";
import VueBus from "vue-bus";
import Vue from "vue"
import Vuetify from "vuetify";
import Axios from "axios";

Vue.use(Vuetify);
Vue.use(VueBus);

window.axios = Axios;

Vue.config.productionTip = false;

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

var app_login = new Vue({
    vuetify: new Vuetify(),
    el: '#app_login',
    components: {LoginPage},
    template: '<LoginPage/>',
    render(h) {
        return h(LoginPage, {
            props: {
                logged_in: this.$el.attributes.logged_in.value === '1',
                settings: JSON.parse(this.$el.attributes.settings.value)
            }
        })
    }
});
