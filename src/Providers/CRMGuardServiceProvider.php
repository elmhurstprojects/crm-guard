<?php

namespace ElmhurstProjects\CRMGuard\Providers;

use ElmhurstProjects\CRMGuard\Http\Middleware\CRMGuard;
use Illuminate\Support\ServiceProvider;

class CRMGuardServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'crm-guard');

        $this->loadRoutesFrom(__DIR__ . '/../Http/Routes/routes.php');

        $this->publishes([
            __DIR__.'/../Config/crm-guard.php' => config_path('crm-guard.php'),
        ], 'crm-guard.config');

        $this->publishes([
            __DIR__.'/../Public' => public_path('crm-guard')
        ], 'crm-guard.public');
    }

    public function register()
    {
        $this->app['router']->aliasMiddleware('guard', CRMGuard::class);
    }
}
