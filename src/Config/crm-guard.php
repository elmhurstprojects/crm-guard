<?php

return [
    'login_url' => '/login',

    'logout_url' => '/logout',

    'dashboard_url' => '/crm/dashboard',

    'login_logo_uri' => null
];