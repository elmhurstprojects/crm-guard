<?php

Route::group(['middleware' => ['web']], function () {
    Route::get(config('crm-guard.login_url'), 'ElmhurstProjects\CRMGuard\Http\Controllers\LoginController@login')->name('crm-guard.login');
    Route::post(config('crm-guard.login_url'), 'ElmhurstProjects\CRMGuard\Http\Controllers\LoginController@loginPost')->name('crm-guard.login.post');
    Route::get(config('crm-guard.logout_url'), 'ElmhurstProjects\CRMGuard\Http\Controllers\LoginController@logout')->name('crm-guard.logout');
});
