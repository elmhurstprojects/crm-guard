<?php

namespace ElmhurstProjects\CRMGuard\Http\Middleware;

use Closure;

class CRMGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$this->isHTTPS($request)) {
            return redirect()->secure($request->getRequestUri());
        }

        if(!$this->isAuthorised()) {
            return redirect()->route('crm-guard.login');
        }

        return $next($request);
    }

    /**
     * Is the user authorised for this section
     * @return mixed
     */
    protected function isAuthorised(){
        return \Auth::check();
    }

    /**
     * Is it an HTTPS request
     * @param $request
     * @return bool
     */
    protected function isHTTPS($request):bool
    {
        if(env('APP_ENV') !== 'production') return true;

        return $request->secure();
    }
}
