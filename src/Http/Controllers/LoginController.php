<?php

namespace ElmhurstProjects\CRMGuard\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login()
    {
        $logged_in = \Auth::check();

        $settings = (object)[
            'login_url' => config('crm-guard.login_url'),
            'dashboard_url' => config('crm-guard.dashboard_url'),
            'login_logo_uri' => config('crm-guard.login_logo_uri')
        ];

        return view('crm-guard::pages.login')->with('logged_in', $logged_in)->with('settings', json_encode($settings));
    }

    public function loginPost(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $authSuccess = \Auth::attempt($credentials, $request->has('remember'));

        if ($authSuccess) {
            $request->session()->regenerate();

            return response()->json([
                'success' => true
            ]);
        }

        return
            response()->json([
                'success' => false,
                'message' => 'Auth failed (or some other message)'
            ]);
    }

    public function logout(Request $request)
    {
        \Auth::logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->route('crm-guard.login');
    }
}
